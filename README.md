

# ------------------------------------------------ Dockerfile ------------------------------------------------ #
FROM node:12.13-alpine as development

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=development

COPY . .

RUN npm run build

FROM node:12.13-alpine as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=production

COPY . .

COPY --from=development /usr/src/app/dist ./dist

CMD ["node", "dist/main"]


# FROM node:12.13-alpine as development
  - Alpine images are lighter (nhẹ hơn), but using them can `have unexpected behavior` (hành vi không mong muốn)

# WORKDIR /usr/src/app
  - After setting `WORKDIR`, each command Docker executes (thực thi) (defined in the `RUN` statement) will be executed in the specified context (ngữ cảnh được chỉ định)

# COPY package*.json ./
# RUN npm install --only=development
# COPY . .
  - First, we copy only `package.json` and `package-lock.json` (if it exists) - copy nó vào WORKDIR được chỉ định ở trên
  - Then we run, in the `WORKDIR` context, the `npm install` command
  - Once it finishes, we copy the rest of our application's file into the Docker container
  - Here we install only `devDependencies` due to the container being used as a "builder" that takes all the necessary (cần thiết) tools to build the application and later send a clean `/dist` folder to the production image 
  - Thứ tự của các câu lệnh ở đây rất quan trọng do cách Docker caches layers. Mỗi câu lệnh trong Dockerfile sẽ tạo ra image layer, layer này sẽ được lưu trong bộ nhớ đệm (cache)
  - If we copied all files at once and then ran `npm install`, each file change would cause Docker to think it should run `npm install` all over again
  - By first copying only `package*.json` files, we are telling Docker that it should run `npm install` and all commands appearing afterwards (xuất hiện sau khi) only when either package.json or package-lock.json files change

# RUN npm run build
  - Make sure the app is built in the /dist folder -> ở image nó sẽ là usr/src/app/dist

# FROM node:12.13-alpine as production
  - By using the `FROM` statement (câu lệnh) again, we are telling Docker that it should create a new, fresh (mới mẻ) image without any connection to the previous one (không có liên quan đến cái trước đó). This time we are naming it `production`

# ARG NODE_ENV=production
# ENV NODE_ENV=${NODE_ENV}
  - Here we are using the `ARG` statement to define the default value for `NODE_ENV`, even though (mặc dù) the default value is only available during the build time (not when we start the application)
  - Then we use the `ENV` statement to set it to either the default value or the user-set value

# WORKDIR /usr/src/app
# COPY package*.json ./
# RUN npm install --only=production
# COPY . .
  - Now this part is exactly the same as the one above (bây giờ phần này giống hệt phần trên), but this time, we are making sure (đảm bảo) that we install only dependencies defined in `dependencies` in `package.json` by using the `--only=production` argument. This way (bằng cách này) we don't install packages such as Typescript that would cause our final image to increase in size (khiến image cuối cùng tăng kích thước)

# COPY --from=development /usr/src/app/dist ./dist
  - ./dist ở đây sẽ là usr/src/app/dist
  - Here we copy the built `/dist` folder from the `development` image. This way we are only getting the /dist directory, without the `devDependencies`, installed in our final image

# CMD ["node", "dist/main"]
  - Here we define the default command to execute when the image is run

=> Nhờ tính năng `multi-stage build`, chúng ta có thể giữ cho `production image` nhẹ nhất có thể, bằng cách giữ tất cả các thứ không cần thiết trong `development image`

# Build the image
  - docker build -t app-name (.)
  - The -t option is for giving our image a name
  - And after run it: docker run -p 3000:3000 app-name




# ------------------------------------------------ docker-compose ------------------------------------------------ # 

version: '3.7'

services:
  main:
    container_name: main
    build:
      context: .
      target: development
    volumes:
      - .:/usr/src/app
      - /usr/src/app/node_modules
    ports:
      - ${SERVER_PORT}:${SERVER_PORT}
      - 9229:9229
    command: npm run start:dev
    env_file:
      - .env
    networks:
      - webnet
    depends_on:
      - redis
      - postgres
  redis:
    container_name: redis
    image: redis:5
    networks:
      - webnet
  postgres:
    container_name: postgres
    image: postgres:12
    networks:
      - webnet
    environment:
      POSTGRES_PASSWORD: ${DB_PASSWORD}
      POSTGRES_USER: ${DB_USERNAME}
      POSTGRES_DB: ${DB_DATABASE_NAME}
      PG_DATA: /var/lib/postgresql/data
    ports:
      - 5432:5432
    volumes:
      - pgdata:/var/lib/postgresql/data
networks:
  webnet:
volumes:
  pgdata:


- Docker-compose is a tool that comes preinstalled (cài đặt sẵn) with Docker. It was specifically (đặc biệt) made to help developers with their local development.

# version: '3.7'
  - First, we specify (chỉ định) that our file uses docker-compose version 3.7. We use this version specifically (đặc biệt) due to its support of multi-stage build
  - Then we define three services: `main`, `redis` and `postgres`

# SERVICES

# main
  - The `main` service is responsible for running our application

container_name: main
build:
  context: .
  target: development
command: npm run start:dev
volumes:
  - .:/usr/src/app
  - /usr/src/app/node_modules
ports:
  - ${SERVER_PORT}:${SERVER_PORT}
  - 9229:9229
env_file:
  - .env
networks:
  - webnet
depends_on:
  - redis
  - postgres

# container_name: main
  - `container_name` tells docker-compose that we will be using the name `main` to refer (tham chiếu) to this service in various docker-compose commands

# build:
#   context: .
#   target: development
  - In the build config, we define the `context`, which (which ở đây là context) tells Docker which files should be sent to Docker deamon (context này sẽ cho Docker biết file nào sẽ được gửi đến Docker deamon ). In our case, that's our whole application (toàn bộ ứng dụng), and so we pass in `(.)`, which mean all of current directory
  - Define a `target` property and set it to `development`. Thanks to this property (nhờ thuộc tính này), Docker will now only build the first part of our Dockerfile and completely ignore (hoàn toàn bỏ qua) the production part of our build (it will stop before the second `FROM` statement) - Có nghĩa là nó chỉ build stage development và không build stage thứ 2 là production

# command: npm run start:dev
  - In our Dockerfile, we defined the commands as `CMD ["node", "dist/main"]`, but this is not a command that we would like to be run in a development environment (nhưng nó không phải là lệnh mà chúng tôi muốn nó chạy trong môi trường phát triển). Instead, we would like to run a process that watches our files and restarts the application after each change. We can do so by using the `command` config
  - The problem with this command is that due to the way Docker works, changing a file on our host machine (our computer) won't be reflected (phản ánh) in the container. Once (sau khi) we copy the files to the container (using the `COPY . .` statement in the Dockerfile), they stay the same (chúng vẫn giữ nguyên, không thay đổi). There is, however, a trick that makes use of `volumes`

# volumes:
#  - .:/usr/src/app
#  - /usr/src/app/node_modules
  - A volume is a way to mount a host directory in a container, and we define two of them
  - The first mounts our current directory `(.)` inside the Docker container `usr/src/app`. This way, when we change a file on our host machine, the file will also be changed in the container - Quá trình này sẽ vẫn chạy bên trong container, nó sẽ restart lại application mỗi khi file change
  - The second volume is a hack. By mounting the first volume in the container, we could accidentally (vô tình) also override the `node_modules` directory with the one we have locally (cục bộ). Developers usually have `node_modules` on their host machine due to the dev tools Visual Studio Code relies on - packages such as `eslint` or `@types`
  - With that in mind (với ý nghĩ đó), we can use an anonymous volume that will prevent (ngăn) the `node_module` existing in the container to ever be overridden - Sử dụng anonymous volume ngăn không cho node_modules tồn tại trong container bị ghi đè

# ports
#   - ${SERVER_PORT:${SERVER:PORT}
#   - 9229:9229
  - Docker's container has ít own network (docker container có mạng riêng của có), so by using ports, we are exposing them to be available to our host machine. The syntax is `HOST_PORT:CONTAINER_PORT`
  - The `${SERVER_PORT}` syntax means that the value will be retrieved (truy xuất) from the environment variables.

# env_file:
#   - .env
  - When working with Node.js application, we normally use the `.env` file to keep our environment variables in one place. Since we are using environment variables in our config (like we do above in `ports`), we also load the variables from the file just in case they were defined (xác định) there
  
# networks:
#   - webnet
  - Since each service (vì mỗi dịch vụ) has its own internal (nội bộ) network (due to their being different containers), we also create our own network that will make it possible for them to communicate (giao tiếp) - chúng ta có thể tạo ra mạng riêng của mình để giúp họ giao tiếp
  - Note that the network is defined at the bottom of the file, here we are just telling docker-compose to use it in this particular service (dịch vụ cụ thể)

# depends_on
#   - redis
#   - postgres
  - When our application starts, we expect (mong đợi) that both the Postgres database and the Redis storage are ready to be used. Otherwise (nếu không), our application would probably (có thể) crash


# redis

redis:
  container_name: redis
  image: redis:5
  networks:
    - webnet
  
- The `redis` config is very simple. First, we define its `container_name`. Then we specify `image` name, which should be fetched from the `respository`. We also have to define the network that is to be used to communicate (giao tiếp) with other services


# postgres

postgres: 
  container_name: postgres
  image: postgres:12
  networks:
    - webnet
  enviroment:
    POSTGRES_PASSWORD: ${DB_PASSWORD}
    POSTGRES_USER: ${DB_USERNAME}
    POSTGRES_DB: ${DB_DATABASE_NAME}
    PG_DATA: /var/lib/postgresql/data
  volumes:
    - pgdata:/var/lib/postgresql/data
  ports:
    - 5432:5432

  - The `postgres` image makes use of a few environment variables that are described (mô tả) in the image's documentation. When we define (định nghĩa) the specified (chỉ định) variables, Postgres will use them (when starting the container) to do certain things (để làm những việc nhất định)
  - Variables like `POSTGRES_PASSWORD`, `POSTGRES_USER`, `POSTGRES_DB` are used to create the default database. Without them (nếu không có chúng), we would have to write the SQL code ourselves (tự viết code) and copy it into the container to create a database - chúng ta sẽ phải tự viết SQL code và copy nó vào container để tạo database.
  - The `PG_DATA` variable is used to tell Postgres where it should keep all the Postgres-related data. We set it to `/var/lib/postgresql/data` (biến PG_DATA được sử dụng để cho Postgres biết nơi nó lưu giữ tất cả dữ liệu liên quan đến Postgres)

# volumes:
#   - pgdata:/var/lib/postgresql/data
  - What may confuse (bối rối) you is that the first part of the volume is not a directory, but rather something called `pgdata` (mà là thư mục được gọi là pgdata)
  - `pgdata` is a named volume that is defined at the bottom of our file
    volumes:
      pgdata:
  - By using a named value, we make sure that the data stays the same even when the container is removed (chúng tôi đảm bảo rằng dữ liệu vẫn giữ nguyên ngay cả khi container bị xoá). It will stay there unil we delete the volume ourselves (nó vẫn ở đó cho đến khi chúng tôi tự xoá tệp)

# ports:
#   5432:5432
  - Finally, we expose the `5432` port, which is the default Postgres port, to make it possible to connect to the database from our host machine with tools such as pgadmin


# Bonus: Adding npm packages
  - In the `main` service config, we defined `node_modules` as an anonymous volume to prevent our host files from overriding the directory. So if we were to add a new npm package by using `npm install`, the package wouldn't be available in the Docker context, and the application would crash
  - Even if (thậm chí nếu) you run `docker-compose donw` and then `docker-compose-up` again in order to start over (để bắt đầu lại), the volume would stay same (không thay đổi). It won't work because anonymous volumes aren't removed until their parent container is removed (không bị xoá cho đến khi parent container chính của chúng bị xoá)
  - To fix this, we can run the following command: `docker-compose up --build -V`
  - The `--build` paremeter will make sure the npm install is run (during the build process), and the -V argument will remove any anonymous volumes and create them again
